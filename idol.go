package main

import (
   "encoding/xml"
   //"bytes"
   "fmt"
   "html/template"
   "io/ioutil"
   "net/http"
   //"net/url"
)

type Page struct {
    Title string
    Body  []byte
}

func (p *Page) save() error {
    filename := p.Title + ".txt"
    return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
    filename := title + ".txt"
    body, err := ioutil.ReadFile(filename)
    if err != nil {
        return nil, err
    }
    return &Page{Title: title, Body: body}, nil
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
    t, _ := template.ParseFiles(tmpl + ".html")
    t.Execute(w, p)
}

type EduceFromTextHit struct {
    Entityname           string `xml:"entity_name"`
    Offset               string `xml:"offset"`
    Offsetlength         string `xml:"offset_length"`
    Score                string `xml:"score"`
    Originaltext         string `xml:"original_text"`
    Originaltextsize     string `xml:"original_text_size"`
    Originaltextlength   string `xml:"original_text_length"`
    Matchedtext          string `xml:"matched_text"`
    Normalizedtextsize   string `xml:"normalized_text_size"`
    Normalizedtextlength string `xml:"normalized_text_length"`
    Normalizedtext       string `xml:"normalized_text"`
}

type EduceFromTextResponseData struct {
    Hits      []EduceFromTextHit `xml:"hit"`
    Numhits   string `xml:"numhits"`
}

type EduceFromTextAutnResponse struct {
    Action    string `xml:"action"`
    Response  string `xml:"response"`
    Responsedata EduceFromTextResponseData `xml:"responsedata"`
}

type CategorySuggestFromTextHit struct {
    Reference string `xml:"reference"`
    Id        string `xml:"id"`
    Weight    string `xml:"weight"`
    Links     string `xml:"links"`
    Title     string `xml:"title"`
}

type CategorySuggestFromTextResponseData struct {
    Numhits   string `xml:"numhits"`
    Hits      []CategorySuggestFromTextHit `xml:"hit"`
}

type CategorySuggestFromTextAutnResponse struct {
    Action    string `xml:"action"`
    Response  string `xml:"response"`
    Responsedata CategorySuggestFromTextResponseData `xml:"responsedata"`
}

func categorySuggestFromTextHandler(w http.ResponseWriter, r *http.Request) {
    title := "CategorySuggest"
    var url, url2 string
    url += "http://localhost:9220/action=categorySuggestFromText"
    url += "&queryText="
    url += r.FormValue("body")
    url2 += "http://localhost:8000/action=EduceFromText"
    url2 += "&Text="
    url2 += r.FormValue("body")
    resp, r_err := http.Get(url)

    if r_err != nil {
        panic(r_err)
    }
    defer resp.Body.Close()

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        fmt.Printf("error: %v", err)
        return
    }
    var autnresponse CategorySuggestFromTextAutnResponse
    u_err := xml.Unmarshal(body, &autnresponse)
    if u_err != nil {
        fmt.Printf("error: %v", u_err)
        return
    }

    p, err := loadPage(title)
    if err != nil {
        p = &Page{Title: title,
            Body: []byte(r.FormValue("body"))}
    }
    renderTemplate(w, "head", p)
    renderTemplate(w, "categorysuggest",p)
    fmt.Fprintf(w, "<table><tr class=\"coltitles\"><td class=\"keyword\">Category Keyword</td><td class=\"weight\">Weight</td><td class=\"links\">Links</td></tr>")
    for _,element := range autnresponse.Responsedata.Hits {
        fmt.Fprintf(w, "<tr><td>%v</td><td>%v</td><td>%v</td></tr>",
            element.Title, element.Weight, element.Links)
    }
    fmt.Fprintf(w, "</table>")

    resp2, r_err2 := http.Get(url2)

    if r_err2 != nil {
        panic(r_err2)
    }
    defer resp2.Body.Close()
    body2, err2 := ioutil.ReadAll(resp2.Body)
    if err2 != nil {
        fmt.Printf("error: %v", err2)
        return
    }
    var autnresponse2 EduceFromTextAutnResponse
    u_err2 := xml.Unmarshal(body2, &autnresponse2)
    if u_err2 != nil {
        fmt.Printf("error: %v", u_err2)
        return
    }

    fmt.Fprintf(w, "<table><tr class=\"coltitles\"><td>Eduction Keywords</td></tr>")
    for _,element := range autnresponse2.Responsedata.Hits {
        fmt.Fprintf(w, "<tr><td>%v</td></tr>",
            element.Normalizedtext)
    }
    fmt.Fprintf(w, "</table>")

    fmt.Fprintf(w, "</table></div></body>")

}

func viewHandler(w http.ResponseWriter, r *http.Request) {
    title := r.URL.Path[len("/view/"):]
    p, err := loadPage(title)
    if err != nil {
        http.Redirect(w, r, "/edit/"+title, http.StatusFound)
        return
    }
    //fmt.Fprintf(w, "<h1>%s</h1><div>%s</div>", p.Title, p.Body)
    renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request) {
    title := r.URL.Path[len("/edit/"):]
    p, err := loadPage(title)
    if err != nil {
        p = &Page{Title: title}
    }
    renderTemplate(w, "edit", p)
}

func queryHandler(w http.ResponseWriter, r *http.Request) {
    title := "query"
    p, err := loadPage(title)
    if err != nil {
        p = &Page{Title: title}
    }
    renderTemplate(w, "head", p)
    renderTemplate(w, "query", p)
}

func main() {
    http.HandleFunc("/view/", viewHandler)
    http.HandleFunc("/edit/", editHandler)
    http.HandleFunc("/query/", queryHandler)
    http.HandleFunc("/categorySuggestFromText/", categorySuggestFromTextHandler)
    //http.HandleFunc("/save/", saveHandler)
    http.Handle("/public/", http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))
    http.ListenAndServe(":8888", nil)
}


